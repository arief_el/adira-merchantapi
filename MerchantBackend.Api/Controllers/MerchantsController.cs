﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;

namespace MerchantBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerchantsController : ControllerBase
    {
        private readonly AdiraLoyaltyContext _context;

        public MerchantsController(AdiraLoyaltyContext context)
        {
            _context = context;
        }

        // GET: api/Merchants
        [HttpGet(Name = "Get Merchant List")]
        public async Task<ActionResult<IEnumerable<Models.Merchant>>> GetMerchant()
        {
            return await _context.Merchant.ToListAsync();
        }

        // GET: api/Merchants/5
        [HttpGet("{id}",Name = "Get Merchant Details")]
        public async Task<ActionResult<Models.Merchant>> GetMerchant(Guid id)
        {
            var merchant = await _context.Merchant.FindAsync(id);

            if (merchant == null)
            {
                return NotFound();
            }

            return merchant;
        }

        // PUT: api/Merchants/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}",Name = "Edit Merchant")]
        public async Task<IActionResult> PutMerchant(Guid id, Models.Merchant merchant)
        {
            if (id != merchant.McId)
            {
                return BadRequest();
            }

            _context.Entry(merchant).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MerchantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Merchants
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost(Name = "Insert New Merchant")]
        public async Task<ActionResult<Models.Merchant>> PostMerchant(Models.Merchant merchant)
        {
            _context.Merchant.Add(merchant);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMerchant", new { id = merchant.McId }, merchant);
        }

        // DELETE: api/Merchants/5
        [HttpDelete("{id}", Name = "Delete Merchant")]
        public async Task<ActionResult<Models.Merchant>> DeleteMerchant(Guid id)
        {
            var merchant = await _context.Merchant.FindAsync(id);
            if (merchant == null)
            {
                return NotFound();
            }

            _context.Merchant.Remove(merchant);
            await _context.SaveChangesAsync();

            return merchant;
        }

        private bool MerchantExists(Guid id)
        {
            return _context.Merchant.Any(e => e.McId == id);
        }
    }
}
