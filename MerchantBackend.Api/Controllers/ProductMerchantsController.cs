﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;
using Microsoft.EntityFrameworkCore.Migrations.Operations;

namespace MerchantBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductMerchantsController : ControllerBase
    {
        private readonly AdiraLoyaltyContext _context;

        public ProductMerchantsController(AdiraLoyaltyContext context)
        {
            _context = context;
        }

        // GET: api/ProductMerchants
        [HttpGet (Name = "Get ProductMerchant List")]
        public async Task<ActionResult<IEnumerable<ProductMerchant>>> GetProductMerchant()
        {
            return await _context.ProductMerchant.ToListAsync();
        }

        // GET: api/ProductMerchants/5
        [HttpGet("{id}",Name = "Get ProductMerchant Details")]
        public async Task<ActionResult<ProductMerchant>> GetProductMerchant(Guid id)
        {
            var productMerchant = await _context.ProductMerchant.FindAsync(id);

            if (productMerchant == null)
            {
                return NotFound();
            }

            return productMerchant;
        }

        // PUT: api/ProductMerchants/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}", Name = "Edit ProductMerchant")]
        public async Task<IActionResult> PutProductMerchant(Guid id, ProductMerchant productMerchant)
        {
            if (id != productMerchant.ProdmcId)
            {
                return BadRequest();
            }

            _context.Entry(productMerchant).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductMerchantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProductMerchants
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost (Name = "Insert New ProductMerchant")]
        public async Task<ActionResult<ProductMerchant>> PostProductMerchant(ProductMerchant productMerchant)
        {
            _context.ProductMerchant.Add(productMerchant);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProductMerchant", new { id = productMerchant.ProdmcId }, productMerchant);
        }

        // DELETE: api/ProductMerchants/5
        [HttpDelete("{id}", Name = "Delete ProductMerchant")]
        public async Task<ActionResult<ProductMerchant>> DeleteProductMerchant(Guid id)
        {
            var productMerchant = await _context.ProductMerchant.FindAsync(id);
            if (productMerchant == null)
            {
                return NotFound();
            }

            _context.ProductMerchant.Remove(productMerchant);
            await _context.SaveChangesAsync();

            return productMerchant;
        }

        private bool ProductMerchantExists(Guid id)
        {
            return _context.ProductMerchant.Any(e => e.ProdmcId == id);
        }
    }
}
