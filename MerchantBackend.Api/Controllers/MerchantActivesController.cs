﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;

namespace MerchantBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerchantActivesController : ControllerBase
    {
        private readonly AdiraLoyaltyContext _context;

        public MerchantActivesController(AdiraLoyaltyContext context)
        {
            _context = context;
        }

        // GET: api/MerchantActives
        [HttpGet("{skip}/{take}", Name = "Get MerchantActives List") ]
        public async Task<ActionResult<IEnumerable<MerchantActive>>> GetMerchantActive(int skip , int take)
        {
            return await _context.MerchantActive.Skip(skip).Take(take).ToListAsync();
        }

        // GET: api/MerchantActives/5
        [HttpGet("{id}", Name= "Get MerchantActives Details")]
        public async Task<ActionResult<MerchantActive>> GetMerchantActive(Guid id)
        {
            var merchantActive = await _context.MerchantActive.FindAsync(id);

            if (merchantActive == null)
            {
                return NotFound();
            }

            return merchantActive;
        }

        // PUT: api/MerchantActives/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}", Name = "Edit MerchantActives")]
        public async Task<IActionResult> PutMerchantActive(Guid id, MerchantActive merchantActive)
        {
            if (id != merchantActive.McaId)
            {
                return BadRequest();
            }

            _context.Entry(merchantActive).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MerchantActiveExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MerchantActives
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost(Name = "Insert New MerchantActives")]
        public async Task<ActionResult<MerchantActive>> PostMerchantActive(MerchantActive merchantActive)
        {
            _context.MerchantActive.Add(merchantActive);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMerchantActive", new { id = merchantActive.McaId }, merchantActive);
        }

        // DELETE: api/MerchantActives/5
        [HttpDelete("{id}", Name = "Delete MerchantActives")]
        public async Task<ActionResult<MerchantActive>> DeleteMerchantActive(Guid id)
        {
            var merchantActive = await _context.MerchantActive.FindAsync(id);
            if (merchantActive == null)
            {
                return NotFound();
            }

            _context.MerchantActive.Remove(merchantActive);
            await _context.SaveChangesAsync();

            return merchantActive;
        }

        private bool MerchantActiveExists(Guid id)
        {
            return _context.MerchantActive.Any(e => e.McaId == id);
        }
    }
}
