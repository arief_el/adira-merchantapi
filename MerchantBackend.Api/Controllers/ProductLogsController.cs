﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;

namespace MerchantBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductLogsController : ControllerBase
    {
        private readonly AdiraLoyaltyContext _context;

        public ProductLogsController(AdiraLoyaltyContext context)
        {
            _context = context;
        }

        // GET: api/ProductLogs
        [HttpGet (Name = "Get ProductLog List")]
        public async Task<ActionResult<IEnumerable<ProductLog>>> GetProductLog()
        {
            return await _context.ProductLog.ToListAsync();
        }

        // GET: api/ProductLogs/5
        [HttpGet("{id}", Name = "Get ProductLog Details")]
        public async Task<ActionResult<ProductLog>> GetProductLog(Guid id)
        {
            var productLog = await _context.ProductLog.FindAsync(id);

            if (productLog == null)
            {
                return NotFound();
            }

            return productLog;
        }

        // PUT: api/ProductLogs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}",Name = "Edit ProductLog")]
        public async Task<IActionResult> PutProductLog(Guid id, ProductLog productLog)
        {
            if (id != productLog.ProdhId)
            {
                return BadRequest();
            }

            _context.Entry(productLog).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductLogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProductLogs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost(Name = "Insert New ProductLog")]
        public async Task<ActionResult<ProductLog>> PostProductLog(ProductLog productLog)
        {
            _context.ProductLog.Add(productLog);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProductLog", new { id = productLog.ProdhId }, productLog);
        }

        // DELETE: api/ProductLogs/5
        [HttpDelete("{id}",Name ="Delete Product")]
        public async Task<ActionResult<ProductLog>> DeleteProductLog(Guid id)
        {
            var productLog = await _context.ProductLog.FindAsync(id);
            if (productLog == null)
            {
                return NotFound();
            }

            _context.ProductLog.Remove(productLog);
            await _context.SaveChangesAsync();

            return productLog;
        }

        private bool ProductLogExists(Guid id)
        {
            return _context.ProductLog.Any(e => e.ProdhId == id);
        }
    }
}
