﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;

namespace MerchantBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerchantLogsController : ControllerBase
    {
        private readonly AdiraLoyaltyContext _context;

        public MerchantLogsController(AdiraLoyaltyContext context)
        {
            _context = context;
        }

        // GET: api/MerchantLogs
        [HttpGet("{skip}/{take}",Name = "Get MerchantLog List")]
        public async Task<ActionResult<IEnumerable<MerchantLog>>> GetMerchantLog(int skip, int take)
        {
            return await _context.MerchantLog.Skip(skip).Take(take).ToListAsync();
        }

        // GET: api/MerchantLogs/5
        [HttpGet("{id}",Name = "Get MerchantLog Details")]
        public async Task<ActionResult<MerchantLog>> GetMerchantLog(Guid id)
        {
            var merchantLog = await _context.MerchantLog.FindAsync(id);

            if (merchantLog == null)
            {
                return NotFound();
            }

            return merchantLog;
        }

        // PUT: api/MerchantLogs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}",Name = "Edit MerchantLog")]
        public async Task<IActionResult> PutMerchantLog(Guid id, MerchantLog merchantLog)
        {
            if (id != merchantLog.MhId)
            {
                return BadRequest();
            }

            _context.Entry(merchantLog).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MerchantLogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MerchantLogs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost(Name = "Insert New MerchantLog")]
        public async Task<ActionResult<MerchantLog>> PostMerchantLog(MerchantLog merchantLog)
        {
            _context.MerchantLog.Add(merchantLog);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMerchantLog", new { id = merchantLog.MhId }, merchantLog);
        }

        // DELETE: api/MerchantLogs/5
        [HttpDelete("{id}",Name = "Delete MerchantLog")]
        public async Task<ActionResult<MerchantLog>> DeleteMerchantLog(Guid id)
        {
            var merchantLog = await _context.MerchantLog.FindAsync(id);
            if (merchantLog == null)
            {
                return NotFound();
            }

            _context.MerchantLog.Remove(merchantLog);
            await _context.SaveChangesAsync();

            return merchantLog;
        }

        private bool MerchantLogExists(Guid id)
        {
            return _context.MerchantLog.Any(e => e.MhId == id);
        }
    }
}
