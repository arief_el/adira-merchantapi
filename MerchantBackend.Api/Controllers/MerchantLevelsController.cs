﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;

namespace MerchantBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerchantLevelsController : ControllerBase
    {
        private readonly AdiraLoyaltyContext _context;

        public MerchantLevelsController(AdiraLoyaltyContext context)
        {
            _context = context;
        }

        // GET: api/MerchantLevels
        [HttpGet("{skip}/{take} ",Name = "Get MerchantLevel List")]
        public async Task<ActionResult<IEnumerable<MerchantLevel>>> GetMerchantLevel(int skip, int take)
        {
            return await _context.MerchantLevel.Skip(skip).Take(take).ToListAsync();
        }

        // GET: api/MerchantLevels/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MerchantLevel>> GetMerchantLevel(Guid id)
        {
            var merchantLevel = await _context.MerchantLevel.FindAsync(id);

            if (merchantLevel == null)
            {
                return NotFound();
            }

            return merchantLevel;
        }

        // PUT: api/MerchantLevels/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}", Name = "Edit MerchantLevel")]
        public async Task<IActionResult> PutMerchantLevel(Guid id, MerchantLevel merchantLevel)
        {
            if (id != merchantLevel.MclId)
            {
                return BadRequest();
            }

            _context.Entry(merchantLevel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MerchantLevelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MerchantLevels
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost (Name = "Insert New MerchantLevel")]
        public async Task<ActionResult<MerchantLevel>> PostMerchantLevel(MerchantLevel merchantLevel)
        {
            _context.MerchantLevel.Add(merchantLevel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMerchantLevel", new { id = merchantLevel.MclId }, merchantLevel);
        }

        // DELETE: api/MerchantLevels/5
        [HttpDelete("{id}",Name = "Delete MerchantLevel")]
        public async Task<ActionResult<MerchantLevel>> DeleteMerchantLevel(Guid id)
        {
            var merchantLevel = await _context.MerchantLevel.FindAsync(id);
            if (merchantLevel == null)
            {
                return NotFound();
            }

            _context.MerchantLevel.Remove(merchantLevel);
            await _context.SaveChangesAsync();

            return merchantLevel;
        }

        private bool MerchantLevelExists(Guid id)
        {
            return _context.MerchantLevel.Any(e => e.MclId == id);
        }
    }
}
