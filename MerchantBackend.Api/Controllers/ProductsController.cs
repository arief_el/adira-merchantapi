﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;

namespace MerchantBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly AdiraLoyaltyContext _context;

        public ProductsController(AdiraLoyaltyContext context)
        {
            _context = context;
        }

        // GET: api/Products
        [HttpGet("{skip}/{take}",Name = "Get Products List")]
        public async Task<ActionResult<IEnumerable<Product>>> GetProduct(int skip, int take)
        {
            return await _context.Product.Skip(skip).Take(take).ToListAsync();
        }

        // GET: api/Products/5
        [HttpGet("{id}", Name = "Get Product Details")]
        public async Task<ActionResult<Product>> GetProduct(Guid id)
        {
            var product = await _context.Product.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        // GET: api/Products/5
        [HttpPost("Prezent/Filter/{skip}/{take}", Name = "Get Product Prezent List with Filter")]
        public async Task<ActionResult<List<Product>>> GetProductPrezent(ProductPrezentSearch prezentSearch, int skip, int take)
        {
            var query = _context.Product.Where(c => c.ProdVendor == "Prezent" && c.ProdStatus == 1);

            if (prezentSearch.Category.Length > 0)
            {
                query = query.Where(c => c.ProdCategory.ToLower().Contains(prezentSearch.Category.ToLower()));
            }

            if (prezentSearch.Title.Length > 0)
            {
                query = query.Where(c => c.ProdName.ToLower().Contains(prezentSearch.Title.ToLower()));
            }

            List< Product > products = await query.Skip(skip).Take(take).ToListAsync();

            return products;
        }

        [HttpGet("Prezent/{skip}/{take}", Name = "Get Product Prezent List")]
        public async Task<ActionResult<List<Product>>> GetProductPrezent(int skip, int take)
        {

            List<Product> products = await _context.Product.Where(c => c.ProdVendor == "Prezent" && c.ProdStatus == 1).Skip(skip).Take(take).ToListAsync();

            return products;
        }


        // PUT: api/Products/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}", Name = "Edit Product")]
        public async Task<IActionResult> PutProduct(Guid id, Product product)
        {
            if (id != product.ProdId)
            {
                return BadRequest();
            }

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Products
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost (Name = "Insert New Product")]
        public async Task<ActionResult<Product>> PostProduct(Product product)
        {
            _context.Product.Add(product);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProduct", new { id = product.ProdId }, product);
        }

        [HttpPost("Bulk",Name = "Insert New Product Bulk")]
        public async Task<ActionResult<List<Product>>> PostProductBulk(List<Product> products)
        {
            try
            {
                _context.Database.BeginTransaction();

                foreach (Product product in products)
                {
                    int productCount = await _context.Product.Where(c => c.ProdVendor == "Prezent" && c.ProdCode == product.ProdCode).CountAsync();
                    if (productCount <= 0)
                    {
                     _context.Product.Add(product);
                    }
                }

                await _context.SaveChangesAsync();
                _context.Database.CommitTransaction();

                return products;
            }
            catch(Exception ex)
            {
                _context.Database.RollbackTransaction();
                throw ex;
            }
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}",Name = "Delete Products")]
        public async Task<ActionResult<Product>> DeleteProduct(Guid id)
        {
            var product = await _context.Product.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Product.Remove(product);
            await _context.SaveChangesAsync();

            return product;
        }

        private bool ProductExists(Guid id)
        {
            return _context.Product.Any(e => e.ProdId == id);
        }
    }
}
