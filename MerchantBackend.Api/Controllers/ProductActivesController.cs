﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;

namespace MerchantBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductActivesController : ControllerBase
    {
        private readonly AdiraLoyaltyContext _context;

        public ProductActivesController(AdiraLoyaltyContext context)
        {
            _context = context;
        }

        // GET: api/ProductActives
        [HttpGet(Name = "Get ProductActive List")]
        public async Task<ActionResult<IEnumerable<ProductActive>>> GetProductActive()
        {
            return await _context.ProductActive.ToListAsync();
        }

        // GET: api/ProductActives/5
        [HttpGet("{id}",Name = "Get ProductActive Details")]
        public async Task<ActionResult<ProductActive>> GetProductActive(Guid id)
        {
            var productActive = await _context.ProductActive.FindAsync(id);

            if (productActive == null)
            {
                return NotFound();
            }

            return productActive;
        }

        // PUT: api/ProductActives/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}", Name = "Edit ProductActive")]
        public async Task<IActionResult> PutProductActive(Guid id, ProductActive productActive)
        {
            if (id != productActive.ProdaId)
            {
                return BadRequest();
            }

            _context.Entry(productActive).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductActiveExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProductActives
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost (Name = "Insert ProductActive")]
        public async Task<ActionResult<ProductActive>> PostProductActive(ProductActive productActive)
        {
            _context.ProductActive.Add(productActive);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProductActive", new { id = productActive.ProdaId }, productActive);
        }

        // DELETE: api/ProductActives/5
        [HttpDelete("{id}",Name = "Delete ProductActive")]
        public async Task<ActionResult<ProductActive>> DeleteProductActive(Guid id)
        {
            var productActive = await _context.ProductActive.FindAsync(id);
            if (productActive == null)
            {
                return NotFound();
            }

            _context.ProductActive.Remove(productActive);
            await _context.SaveChangesAsync();

            return productActive;
        }

        private bool ProductActiveExists(Guid id)
        {
            return _context.ProductActive.Any(e => e.ProdaId == id);
        }
    }
}
