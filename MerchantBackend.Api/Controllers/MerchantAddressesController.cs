﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;

namespace MerchantBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerchantAddressesController : ControllerBase
    {
        private readonly AdiraLoyaltyContext _context;

        public MerchantAddressesController(AdiraLoyaltyContext context)
        {
            _context = context;
        }

        // GET: api/MerchantAddresses
        [HttpGet ("{skip}/{take}",Name = "Get MerchantAddress List")]
        public async Task<ActionResult<IEnumerable<MerchantAddress>>> GetMerchantAddress(int skip , int take )
        {
            return await _context.MerchantAddress.Skip(skip).Take(take).ToListAsync();
        }

        // GET: api/MerchantAddresses/5
        [HttpGet("{id}",Name = "Get MerchantAddress Details")]
        public async Task<ActionResult<MerchantAddress>> GetMerchantAddress(Guid id)
        {
            var merchantAddress = await _context.MerchantAddress.FindAsync(id);

            if (merchantAddress == null)
            {
                return NotFound();
            }

            return merchantAddress;
        }

        // PUT: api/MerchantAddresses/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}", Name = "Edit MerchantAddress")]
        public async Task<IActionResult> PutMerchantAddress(Guid id, MerchantAddress merchantAddress)
        {
            if (id != merchantAddress.McdId)
            {
                return BadRequest();
            }

            _context.Entry(merchantAddress).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MerchantAddressExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MerchantAddresses
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost(Name = "Insert MerchantAddress")]
        public async Task<ActionResult<MerchantAddress>> PostMerchantAddress(MerchantAddress merchantAddress)
        {
            _context.MerchantAddress.Add(merchantAddress);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMerchantAddress", new { id = merchantAddress.McdId }, merchantAddress);
        }

        // DELETE: api/MerchantAddresses/5
        [HttpDelete("{id}", Name = "Delete MerchantAddress")]
        public async Task<ActionResult<MerchantAddress>> DeleteMerchantAddress(Guid id)
        {
            var merchantAddress = await _context.MerchantAddress.FindAsync(id);
            if (merchantAddress == null)
            {
                return NotFound();
            }

            _context.MerchantAddress.Remove(merchantAddress);
            await _context.SaveChangesAsync();

            return merchantAddress;
        }

        private bool MerchantAddressExists(Guid id)
        {
            return _context.MerchantAddress.Any(e => e.McdId == id);
        }
    }
}
