﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;

namespace MerchantBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerchantIdsController : ControllerBase
    {
        private readonly AdiraLoyaltyContext _context;

        public MerchantIdsController(AdiraLoyaltyContext context)
        {
            _context = context;
        }

        // GET: api/MerchantIds
        [HttpGet (Name = "Get MerchantId List ")]
        public async Task<ActionResult<IEnumerable<MerchantId>>> GetMerchantId()
        {
            return await _context.MerchantId.ToListAsync();
        }

        // GET: api/MerchantIds/5
        [HttpGet("{id}",Name = "Get MerchantId Details")]
        public async Task<ActionResult<MerchantId>> GetMerchantId(Guid id)
        {
            var merchantId = await _context.MerchantId.FindAsync(id);

            if (merchantId == null)
            {
                return NotFound();
            }

            return merchantId;
        }

        // PUT: api/MerchantIds/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}",Name = "Edit MerchantId")]
        public async Task<IActionResult> PutMerchantId(Guid id, MerchantId merchantId)
        {
            if (id != merchantId.MciId)
            {
                return BadRequest();
            }

            _context.Entry(merchantId).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MerchantIdExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MerchantIds
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost(Name = "Insert New MerchantId")]
        public async Task<ActionResult<MerchantId>> PostMerchantId(MerchantId merchantId)
        {
            _context.MerchantId.Add(merchantId);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMerchantId", new { id = merchantId.MciId }, merchantId);
        }

        // DELETE: api/MerchantIds/5
        [HttpDelete("{id}", Name = "Delete MerchantId")]
        public async Task<ActionResult<MerchantId>> DeleteMerchantId(Guid id)
        {
            var merchantId = await _context.MerchantId.FindAsync(id);
            if (merchantId == null)
            {
                return NotFound();
            }

            _context.MerchantId.Remove(merchantId);
            await _context.SaveChangesAsync();

            return merchantId;
        }

        private bool MerchantIdExists(Guid id)
        {
            return _context.MerchantId.Any(e => e.MciId == id);
        }
    }
}
