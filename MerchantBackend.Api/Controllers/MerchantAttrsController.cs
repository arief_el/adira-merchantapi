﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;

namespace MerchantBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerchantAttrsController : ControllerBase
    {
        private readonly AdiraLoyaltyContext _context;

        public MerchantAttrsController(AdiraLoyaltyContext context)
        {
            _context = context;
        }

        // GET: api/MerchantAttrs
        [HttpGet("{skip}/{take}", Name = "Get MerchantAttr List")]
        public async Task<ActionResult<IEnumerable<MerchantAttr>>> GetMerchantAttr(int skip, int take)
        {
            return await _context.MerchantAttr.Skip(skip).Take(take).ToListAsync();
        }

        // GET: api/MerchantAttrs/5
        [HttpGet("{id}", Name = "Get MerchantAttr Details")]
        public async Task<ActionResult<MerchantAttr>> GetMerchantAttr(Guid id)
        {
            var merchantAttr = await _context.MerchantAttr.FindAsync(id);

            if (merchantAttr == null)
            {
                return NotFound();
            }

            return merchantAttr;
        }

        // PUT: api/MerchantAttrs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}", Name = "Edit MerchantAttr ")]
        public async Task<IActionResult> PutMerchantAttr(Guid id, MerchantAttr merchantAttr)
        {
            if (id != merchantAttr.MctId)
            {
                return BadRequest();
            }

            _context.Entry(merchantAttr).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MerchantAttrExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MerchantAttrs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost (Name = "Insert New MerchantAttr")]
        public async Task<ActionResult<MerchantAttr>> PostMerchantAttr(MerchantAttr merchantAttr)
        {
            _context.MerchantAttr.Add(merchantAttr);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMerchantAttr", new { id = merchantAttr.MctId }, merchantAttr);
        }

        // DELETE: api/MerchantAttrs/5
        [HttpDelete("{id}",Name = "Delete MerchantAttr")]
        public async Task<ActionResult<MerchantAttr>> DeleteMerchantAttr(Guid id)
        {
            var merchantAttr = await _context.MerchantAttr.FindAsync(id);
            if (merchantAttr == null)
            {
                return NotFound();
            }

            _context.MerchantAttr.Remove(merchantAttr);
            await _context.SaveChangesAsync();

            return merchantAttr;
        }

        private bool MerchantAttrExists(Guid id)
        {
            return _context.MerchantAttr.Any(e => e.MctId == id);
        }
    }
}
