﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class ProductMerchant
    {
        public Guid ProdmcId { get; set; }
        public Guid? ProdId { get; set; }
        public Guid? McId { get; set; }
        public short? ProdmcStatus { get; set; }
        public string ProdmcStampToken { get; set; }
        public string ProdmcUser { get; set; }
    }
}
