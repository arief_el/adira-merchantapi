﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class MerchantActive
    {
        public Guid McaId { get; set; }
        public Guid? McaClient { get; set; }
        public Guid? McId { get; set; }
        public string McaStatus { get; set; }
        public DateTimeOffset? McaDate { get; set; }
        public string McaNote { get; set; }
        public short? McaActive { get; set; }
        public string McaStampToken { get; set; }
        public string McaUser { get; set; }

        public virtual Merchant Mc { get; set; }
    }
}
