﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class ProductLog
    {
        public Guid ProdhId { get; set; }
        public Guid? ProdhClientId { get; set; }
        public string ProdhRequestId { get; set; }
        public string ProdhRequestData { get; set; }
        public DateTime? ProdhRequestTime { get; set; }
        public string ProdhResponseId { get; set; }
        public string ProdhResponseData { get; set; }
        public DateTime? ProdhResponseTime { get; set; }
        public string ProdhUser { get; set; }
    }
}
