﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class Merchant
    {
        public Merchant()
        {
            MerchantActive = new HashSet<MerchantActive>();
            MerchantAddress = new HashSet<MerchantAddress>();
            MerchantId = new HashSet<MerchantId>();
            MerchantLevel = new HashSet<MerchantLevel>();
        }

        public Guid McId { get; set; }
        public Guid? McClientId { get; set; }
        public Guid? McOrganization { get; set; }
        public Guid? McParent { get; set; }
        public string McName { get; set; }
        public string McArea { get; set; }
        public short? McActive { get; set; }
        public string McStampToken { get; set; }
        public string McUser { get; set; }

        public virtual Client McClient { get; set; }
        public virtual ICollection<MerchantActive> MerchantActive { get; set; }
        public virtual ICollection<MerchantAddress> MerchantAddress { get; set; }
        public virtual ICollection<MerchantId> MerchantId { get; set; }
        public virtual ICollection<MerchantLevel> MerchantLevel { get; set; }
    }
}
