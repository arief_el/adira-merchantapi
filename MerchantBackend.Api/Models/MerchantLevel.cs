﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class MerchantLevel
    {
        public Guid MclId { get; set; }
        public Guid? MclClient { get; set; }
        public Guid? McId { get; set; }
        public string MclCode { get; set; }
        public DateTimeOffset? MclDate { get; set; }
        public string MclNote { get; set; }
        public short? MclActive { get; set; }
        public string MclStampToken { get; set; }
        public string MclUser { get; set; }

        public virtual Merchant Mc { get; set; }
    }
}
