﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class Product
    {
        public Guid ProdId { get; set; }
        public Guid? ProdClientId { get; set; }
        public Guid? ProdOrganization { get; set; }
        public Guid? ProdParent { get; set; }
        public short? ProdType { get; set; }
        public string ProdCode { get; set; }
        public string ProdQrCode { get; set; }
        public string ProdBarCode { get; set; }
        public string ProdStandardCode { get; set; }
        public string ProdName { get; set; }
        public string ProdDesc { get; set; }
        public string ProdPictures { get; set; }
        public string ProdPrice { get; set; }
        public string ProdCategory { get; set; }
        public string ProdCatalogue { get; set; }
        public string ProdVendor { get; set; }
        public short? ProdStatus { get; set; }
        public string ProdStampToken { get; set; }
        public string ProdUser { get; set; }
    }

    public partial class ProductPrezentSearch
    {
        public string Title { get; set; }
        public string Category { get; set; }
    }
}