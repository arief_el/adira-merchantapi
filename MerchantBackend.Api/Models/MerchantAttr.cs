﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class MerchantAttr
    {
        public Guid MctId { get; set; }
        public Guid? MctClient { get; set; }
        public Guid? McId { get; set; }
        public string MctCode { get; set; }
        public string MctName { get; set; }
        public string MctValueString { get; set; }
        public decimal? MctValueNumeric { get; set; }
        public short? MctActive { get; set; }
        public Guid? MctLock { get; set; }
        public string MctStampToken { get; set; }
        public string MctUser { get; set; }
    }
}
