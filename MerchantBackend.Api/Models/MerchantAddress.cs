﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class MerchantAddress
    {
        public Guid McdId { get; set; }
        public Guid? McdClient { get; set; }
        public Guid? McId { get; set; }
        public short? McdPriority { get; set; }
        public string McdType { get; set; }
        public string McdCountry { get; set; }
        public string McdProvince { get; set; }
        public string McdCity { get; set; }
        public string McdZip { get; set; }
        public string McdAddress { get; set; }
        public string McdNote { get; set; }
        public short? McdActive { get; set; }
        public string McdStampToken { get; set; }
        public string McdUser { get; set; }

        public virtual Merchant Mc { get; set; }
    }
}
