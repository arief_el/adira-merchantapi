﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class Client
    {
        public Client()
        {
            Merchant = new HashSet<Merchant>();
        }

        public Guid ClId { get; set; }
        public string ClName { get; set; }
        public string ClTheme { get; set; }
        public int? ClCounterSerialPoints { get; set; }
        public int? ClCounterVoucher { get; set; }
        public int? ClCounterTrx { get; set; }
        public int? ClActive { get; set; }
        public Guid? ClLockCounter { get; set; }
        public string ClBaseCurrency { get; set; }
        public Guid? ClBaseAsset { get; set; }
        public Guid? ClBaseIssuer { get; set; }

        public virtual ICollection<Merchant> Merchant { get; set; }
    }
}
