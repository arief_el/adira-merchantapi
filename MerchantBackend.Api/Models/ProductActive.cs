﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class ProductActive
    {
        public Guid ProdaId { get; set; }
        public Guid? ProdaClient { get; set; }
        public Guid? ProdId { get; set; }
        public string ProdaStatus { get; set; }
        public DateTimeOffset? ProdaDate { get; set; }
        public string ProdaNote { get; set; }
        public short? ProdaActive { get; set; }
        public string ProdaStampToken { get; set; }
        public string ProdaUser { get; set; }
    }
}
