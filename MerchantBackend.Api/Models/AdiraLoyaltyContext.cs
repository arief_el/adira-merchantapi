﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MerchantBackend.Models
{
    public partial class AdiraLoyaltyContext : DbContext
    {
        //public AdiraLoyaltyContext()
        //{
        //}

        public AdiraLoyaltyContext(DbContextOptions<AdiraLoyaltyContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Merchant> Merchant { get; set; }
        public virtual DbSet<MerchantActive> MerchantActive { get; set; }
        public virtual DbSet<MerchantAddress> MerchantAddress { get; set; }
        public virtual DbSet<MerchantAttr> MerchantAttr { get; set; }
        public virtual DbSet<MerchantId> MerchantId { get; set; }
        public virtual DbSet<MerchantLevel> MerchantLevel { get; set; }
        public virtual DbSet<MerchantLog> MerchantLog { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductActive> ProductActive { get; set; }
        public virtual DbSet<ProductLog> ProductLog { get; set; }
        public virtual DbSet<ProductMerchant> ProductMerchant { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Data Source=DESKTOP-AG0G4RK\\SQLEXPRESS;Initial Catalog=AdiraLoyalty;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>(entity =>
            {
                entity.HasKey(e => e.ClId);

                entity.Property(e => e.ClId)
                    .HasColumnName("clId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ClActive).HasColumnName("clActive");

                entity.Property(e => e.ClBaseAsset).HasColumnName("clBaseAsset");

                entity.Property(e => e.ClBaseCurrency)
                    .HasColumnName("clBaseCurrency")
                    .HasMaxLength(3);

                entity.Property(e => e.ClBaseIssuer).HasColumnName("clBaseIssuer");

                entity.Property(e => e.ClCounterSerialPoints)
                    .HasColumnName("clCounterSerialPoints")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ClCounterTrx).HasColumnName("clCounterTrx");

                entity.Property(e => e.ClCounterVoucher).HasColumnName("clCounterVoucher");

                entity.Property(e => e.ClLockCounter).HasColumnName("clLockCounter");

                entity.Property(e => e.ClName)
                    .HasColumnName("clName")
                    .HasMaxLength(50);

                entity.Property(e => e.ClTheme)
                    .HasColumnName("clTheme")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Merchant>(entity =>
            {
                entity.HasKey(e => e.McId);

                entity.Property(e => e.McId)
                    .HasColumnName("mcId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.McActive)
                    .HasColumnName("mcActive")
                    .HasDefaultValueSql("((0))")
                    .HasComment("0:inactive, 1:active, 2:suspend, 3:blacklist, 4:deleted");

                entity.Property(e => e.McArea)
                    .HasColumnName("mcArea")
                    .HasMaxLength(20);

                entity.Property(e => e.McClientId).HasColumnName("mcClientId");

                entity.Property(e => e.McName)
                    .HasColumnName("mcName")
                    .HasMaxLength(250);

                entity.Property(e => e.McOrganization).HasColumnName("mcOrganization");

                entity.Property(e => e.McParent).HasColumnName("mcParent");

                entity.Property(e => e.McStampToken)
                    .HasColumnName("mcStampToken")
                    .HasMaxLength(100);

                entity.Property(e => e.McUser)
                    .HasColumnName("mcUser")
                    .HasMaxLength(50);

                entity.HasOne(d => d.McClient)
                    .WithMany(p => p.Merchant)
                    .HasForeignKey(d => d.McClientId)
                    .HasConstraintName("FK_Merchant_Client");
            });

            modelBuilder.Entity<MerchantActive>(entity =>
            {
                entity.HasKey(e => e.McaId);

                entity.Property(e => e.McaId)
                    .HasColumnName("mcaId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.McId).HasColumnName("mcId");

                entity.Property(e => e.McaActive)
                    .HasColumnName("mcaActive")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.McaClient).HasColumnName("mcaClient");

                entity.Property(e => e.McaDate)
                    .HasColumnName("mcaDate")
                    .HasColumnType("datetimeoffset(0)");

                entity.Property(e => e.McaNote).HasColumnName("mcaNote");

                entity.Property(e => e.McaStampToken)
                    .HasColumnName("mcaStampToken")
                    .HasMaxLength(50);

                entity.Property(e => e.McaStatus)
                    .HasColumnName("mcaStatus")
                    .HasMaxLength(10)
                    .HasComment("[register, active, inactive, suspend, blacklist, deleted]");

                entity.Property(e => e.McaUser)
                    .HasColumnName("mcaUser")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Mc)
                    .WithMany(p => p.MerchantActive)
                    .HasForeignKey(d => d.McId)
                    .HasConstraintName("FK_MerchantActive_Merchant");
            });

            modelBuilder.Entity<MerchantAddress>(entity =>
            {
                entity.HasKey(e => e.McdId);

                entity.Property(e => e.McdId)
                    .HasColumnName("mcdId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.McId).HasColumnName("mcId");

                entity.Property(e => e.McdActive).HasColumnName("mcdActive");

                entity.Property(e => e.McdAddress)
                    .HasColumnName("mcdAddress")
                    .HasMaxLength(100);

                entity.Property(e => e.McdCity)
                    .HasColumnName("mcdCity")
                    .HasMaxLength(20);

                entity.Property(e => e.McdClient).HasColumnName("mcdClient");

                entity.Property(e => e.McdCountry)
                    .HasColumnName("mcdCountry")
                    .HasMaxLength(20);

                entity.Property(e => e.McdNote).HasColumnName("mcdNote");

                entity.Property(e => e.McdPriority).HasColumnName("mcdPriority");

                entity.Property(e => e.McdProvince)
                    .HasColumnName("mcdProvince")
                    .HasMaxLength(20);

                entity.Property(e => e.McdStampToken)
                    .HasColumnName("mcdStampToken")
                    .HasMaxLength(50);

                entity.Property(e => e.McdType)
                    .HasColumnName("mcdType")
                    .HasMaxLength(10);

                entity.Property(e => e.McdUser)
                    .HasColumnName("mcdUser")
                    .HasMaxLength(50);

                entity.Property(e => e.McdZip)
                    .HasColumnName("mcdZip")
                    .HasMaxLength(5);

                entity.HasOne(d => d.Mc)
                    .WithMany(p => p.MerchantAddress)
                    .HasForeignKey(d => d.McId)
                    .HasConstraintName("FK_MerchantAddress_Merchant");
            });

            modelBuilder.Entity<MerchantAttr>(entity =>
            {
                entity.HasKey(e => e.MctId);

                entity.Property(e => e.MctId)
                    .HasColumnName("mctId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.McId).HasColumnName("mcId");

                entity.Property(e => e.MctActive)
                    .HasColumnName("mctActive")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MctClient).HasColumnName("mctClient");

                entity.Property(e => e.MctCode)
                    .HasColumnName("mctCode")
                    .HasMaxLength(10)
                    .HasComment("");

                entity.Property(e => e.MctLock).HasColumnName("mctLock");

                entity.Property(e => e.MctName)
                    .HasColumnName("mctName")
                    .HasMaxLength(50);

                entity.Property(e => e.MctStampToken)
                    .HasColumnName("mctStampToken")
                    .HasMaxLength(50);

                entity.Property(e => e.MctUser)
                    .HasColumnName("mctUser")
                    .HasMaxLength(50);

                entity.Property(e => e.MctValueNumeric)
                    .HasColumnName("mctValueNumeric")
                    .HasColumnType("numeric(18, 6)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MctValueString)
                    .HasColumnName("mctValueString")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<MerchantId>(entity =>
            {
                entity.HasKey(e => e.MciId);

                entity.Property(e => e.MciId)
                    .HasColumnName("mciId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.McId).HasColumnName("mcId");

                entity.Property(e => e.MciActive)
                    .HasColumnName("mciActive")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MciClient).HasColumnName("mciClient");

                entity.Property(e => e.MciDate)
                    .HasColumnName("mciDate")
                    .HasColumnType("datetimeoffset(0)");

                entity.Property(e => e.MciIdImage)
                    .HasColumnName("mciIdImage")
                    .HasMaxLength(100);

                entity.Property(e => e.MciIdNo)
                    .HasColumnName("mciIdNo")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('noidno')");

                entity.Property(e => e.MciIdType)
                    .HasColumnName("mciIdType")
                    .HasMaxLength(10)
                    .HasDefaultValueSql("('noidtype')");

                entity.Property(e => e.MciNote).HasColumnName("mciNote");

                entity.Property(e => e.MciStampToken)
                    .HasColumnName("mciStampToken")
                    .HasMaxLength(50);

                entity.Property(e => e.MciUser)
                    .HasColumnName("mciUser")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Mc)
                    .WithMany(p => p.MerchantId)
                    .HasForeignKey(d => d.McId)
                    .HasConstraintName("FK_MerchantId_Merchant");
            });

            modelBuilder.Entity<MerchantLevel>(entity =>
            {
                entity.HasKey(e => e.MclId);

                entity.Property(e => e.MclId)
                    .HasColumnName("mclId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.McId).HasColumnName("mcId");

                entity.Property(e => e.MclActive)
                    .HasColumnName("mclActive")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MclClient).HasColumnName("mclClient");

                entity.Property(e => e.MclCode)
                    .HasColumnName("mclCode")
                    .HasMaxLength(10);

                entity.Property(e => e.MclDate)
                    .HasColumnName("mclDate")
                    .HasColumnType("datetimeoffset(0)");

                entity.Property(e => e.MclNote).HasColumnName("mclNote");

                entity.Property(e => e.MclStampToken)
                    .HasColumnName("mclStampToken")
                    .HasMaxLength(50);

                entity.Property(e => e.MclUser)
                    .HasColumnName("mclUser")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Mc)
                    .WithMany(p => p.MerchantLevel)
                    .HasForeignKey(d => d.McId)
                    .HasConstraintName("FK_MerchantLevel_Merchant");
            });

            modelBuilder.Entity<MerchantLog>(entity =>
            {
                entity.HasKey(e => e.MhId);

                entity.Property(e => e.MhId)
                    .HasColumnName("mhId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.MhClientId).HasColumnName("mhClientId");

                entity.Property(e => e.MhRequestData).HasColumnName("mhRequestData");

                entity.Property(e => e.MhRequestId)
                    .HasColumnName("mhRequestId")
                    .HasMaxLength(50);

                entity.Property(e => e.MhRequestTime)
                    .HasColumnName("mhRequestTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.MhResponseData).HasColumnName("mhResponseData");

                entity.Property(e => e.MhResponseId)
                    .HasColumnName("mhResponseId")
                    .HasMaxLength(50);

                entity.Property(e => e.MhResponseTime)
                    .HasColumnName("mhResponseTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.MhUser)
                    .HasColumnName("mhUser")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(e => e.ProdId);

                entity.Property(e => e.ProdId)
                    .HasColumnName("prodId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ProdBarCode)
                    .HasColumnName("prodBarCode")
                    .HasMaxLength(50);

                entity.Property(e => e.ProdCatalogue).HasColumnName("prodCatalogue");

                entity.Property(e => e.ProdCategory).HasColumnName("prodCategory");

                entity.Property(e => e.ProdClientId).HasColumnName("prodClientId");

                entity.Property(e => e.ProdCode)
                    .HasColumnName("prodCode")
                    .HasMaxLength(20);

                entity.Property(e => e.ProdDesc).HasColumnName("prodDesc");

                entity.Property(e => e.ProdName)
                    .HasColumnName("prodName")
                    .HasMaxLength(100);

                entity.Property(e => e.ProdOrganization).HasColumnName("prodOrganization");

                entity.Property(e => e.ProdParent).HasColumnName("prodParent");

                entity.Property(e => e.ProdPictures).HasColumnName("prodPictures");

                entity.Property(e => e.ProdPrice).HasColumnName("prodPrice");

                entity.Property(e => e.ProdQrCode)
                    .HasColumnName("prodQrCode")
                    .HasMaxLength(1000);

                entity.Property(e => e.ProdStampToken)
                    .HasColumnName("prodStampToken")
                    .HasMaxLength(50);

                entity.Property(e => e.ProdStandardCode).HasColumnName("prodStandardCode");

                entity.Property(e => e.ProdStatus).HasColumnName("prodStatus");

                entity.Property(e => e.ProdType)
                    .HasColumnName("prodType")
                    .HasComment("1: for earning, 2: for redeem, 3: for gift");

                entity.Property(e => e.ProdUser)
                    .HasColumnName("prodUser")
                    .HasMaxLength(50);

                entity.Property(e => e.ProdVendor).HasColumnName("prodVendor");
            });

            modelBuilder.Entity<ProductActive>(entity =>
            {
                entity.HasKey(e => e.ProdaId);

                entity.Property(e => e.ProdaId)
                    .HasColumnName("prodaId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ProdId).HasColumnName("prodId");

                entity.Property(e => e.ProdaActive)
                    .HasColumnName("prodaActive")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ProdaClient).HasColumnName("prodaClient");

                entity.Property(e => e.ProdaDate)
                    .HasColumnName("prodaDate")
                    .HasColumnType("datetimeoffset(0)");

                entity.Property(e => e.ProdaNote).HasColumnName("prodaNote");

                entity.Property(e => e.ProdaStampToken)
                    .HasColumnName("prodaStampToken")
                    .HasMaxLength(50);

                entity.Property(e => e.ProdaStatus)
                    .HasColumnName("prodaStatus")
                    .HasMaxLength(10)
                    .HasComment("[register, active, inactive, suspend, blacklist, deleted, inworkflow]");

                entity.Property(e => e.ProdaUser)
                    .HasColumnName("prodaUser")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ProductLog>(entity =>
            {
                entity.HasKey(e => e.ProdhId);

                entity.Property(e => e.ProdhId)
                    .HasColumnName("prodhId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ProdhClientId).HasColumnName("prodhClientId");

                entity.Property(e => e.ProdhRequestData).HasColumnName("prodhRequestData");

                entity.Property(e => e.ProdhRequestId)
                    .HasColumnName("prodhRequestId")
                    .HasMaxLength(50);

                entity.Property(e => e.ProdhRequestTime)
                    .HasColumnName("prodhRequestTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProdhResponseData).HasColumnName("prodhResponseData");

                entity.Property(e => e.ProdhResponseId)
                    .HasColumnName("prodhResponseId")
                    .HasMaxLength(50);

                entity.Property(e => e.ProdhResponseTime)
                    .HasColumnName("prodhResponseTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProdhUser)
                    .HasColumnName("prodhUser")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ProductMerchant>(entity =>
            {
                entity.HasKey(e => e.ProdmcId);

                entity.Property(e => e.ProdmcId)
                    .HasColumnName("prodmcId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.McId).HasColumnName("mcId");

                entity.Property(e => e.ProdId).HasColumnName("prodId");

                entity.Property(e => e.ProdmcStampToken)
                    .HasColumnName("prodmcStampToken")
                    .HasMaxLength(100);

                entity.Property(e => e.ProdmcStatus).HasColumnName("prodmcStatus");

                entity.Property(e => e.ProdmcUser)
                    .HasColumnName("prodmcUser")
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
