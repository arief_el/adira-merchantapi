﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class MerchantId
    {
        public Guid MciId { get; set; }
        public Guid? MciClient { get; set; }
        public Guid? McId { get; set; }
        public string MciIdType { get; set; }
        public string MciIdNo { get; set; }
        public string MciIdImage { get; set; }
        public DateTimeOffset? MciDate { get; set; }
        public string MciNote { get; set; }
        public short? MciActive { get; set; }
        public string MciStampToken { get; set; }
        public string MciUser { get; set; }

        public virtual Merchant Mc { get; set; }
    }
}
