﻿using System;
using System.Collections.Generic;

namespace MerchantBackend.Models
{
    public partial class MerchantLog
    {
        public Guid MhId { get; set; }
        public Guid? MhClientId { get; set; }
        public string MhRequestId { get; set; }
        public string MhRequestData { get; set; }
        public DateTime? MhRequestTime { get; set; }
        public string MhResponseId { get; set; }
        public string MhResponseData { get; set; }
        public DateTime? MhResponseTime { get; set; }
        public string MhUser { get; set; }
    }
}
