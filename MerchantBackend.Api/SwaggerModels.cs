﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.OpenApi.Models;
using Microsoft.OpenApi.Any;

namespace Merchant.Api
{
    public class AddHeaderOperationFilter : IOperationFilter
    {
        private readonly string parameterName;
        private readonly string description;

        public AddHeaderOperationFilter(string parameterName, string description)
        {
            this.parameterName = parameterName;
            this.description = description;
        }

        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
            {
                operation.Parameters = new List<OpenApiParameter>();
            }

            operation.Parameters.Add(new OpenApiParameter 
            {
                Name = parameterName,
                In = ParameterLocation.Header,
                Description = description,
                Required = true,
                Schema = new OpenApiSchema { Type = "string" }
            });
        }
    }
}
