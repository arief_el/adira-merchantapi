﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Merchant.Api
{
    /// <summary>
    /// Data model for generic response API
    /// </summary>
    public class ReturnStandardModels
    {
        /// <summary>
        /// http status code
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// module status code
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// status message
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Unique Request Id from client
        /// </summary>
        public string ClientRequestId { get; set; }
        /// <summary>
        /// Unique response Id from API
        /// </summary>
        public string ApiResponseId { get; set; }
    }
}
