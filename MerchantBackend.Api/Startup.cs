using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using MerchantBackend.Models;
using Microsoft.OpenApi.Models;
using OAuth.Middleware;
using NLog;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Text;
using Merchant.Api;

namespace MerchantBackend
{
    public class Startup
    {
        private IWebHostEnvironment _appEnv;
        private string _connectionString;
        private List<ApplicationInMemoryClient> _inMemoryClient = new List<ApplicationInMemoryClient>();


        public Startup(IConfiguration configuration, IWebHostEnvironment appEnv)
        {
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
            _appEnv = appEnv;
            _connectionString = Configuration.GetConnectionString("AdiraLoyalty");
            _inMemoryClient.Add(new ApplicationInMemoryClient { clientName = "Adira Loyalty", clientId = new Guid("2270E668-2F02-4953-AB2C-5BFACBF753BB"), clientKey = "<RSAKeyValue><Modulus>iEJK95id6gYB27QhQ4pAVNyPmopuj07D9dvWvbz/6JRvGis/aCmeq19yc5ufy4Z2jYZ+uc/W9jYW8Se/07SwstU4Wn0Bwdy/WuRuUBALVNW+FF14t9pB2VPNP/gmaDP8eiHasSzGFXUqQQED7t7djRVXPyMDWep6Z2nV1AKJi78=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>" });
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                  .AddNewtonsoftJson().AddJsonOptions(option =>
                  {
                      option.JsonSerializerOptions.IgnoreNullValues = true;
                  }); ;

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Product Merchant API", Version = "v1", Description = "Adira Product Merchant API" });
                c.OperationFilter<AddHeaderOperationFilter>("x-api-key", "Your Api Key");
                c.OperationFilter<AddHeaderOperationFilter>("signature", "Signature from payload using your private key");
                c.IncludeXmlComments(System.IO.Path.Combine(_appEnv.ContentRootPath, $"{_appEnv.ApplicationName}.xml"));
            });

            services.AddSingleton<ILog, LogNLog>();

            services.AddDbContext<AdiraLoyaltyContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("AdiraLoyalty")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILog logger) //ILog logger
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            else
            {
                app.UseExceptionHandler(errorApp =>
                {
                    errorApp.Run(async context =>
                    {
                        string Message = "Something went wrong";
                        string Code = "50001";
                        string Status = "500";
                        context.Response.StatusCode = 500;
                        context.Response.ContentType = "application/json";

                        var exceptionHandlerPathFeature =
                            context.Features.Get<Microsoft.AspNetCore.Diagnostics.IExceptionHandlerPathFeature>();

                        // Use exceptionHandlerPathFeature to process the exception (for example, 
                        // logging), but do NOT expose sensitive error information directly to 
                        // the client.

                        logger.Error($"ID:{context.Response.Headers["RequestId"]} From:{context.Connection.RemoteIpAddress} Method:{context.Request.Method} Path:{context.Request.Path} {context.Request.Host} {exceptionHandlerPathFeature.Path} {exceptionHandlerPathFeature.Error.Message} {exceptionHandlerPathFeature.Error.Source} {exceptionHandlerPathFeature.Error.StackTrace}");

                        if (exceptionHandlerPathFeature?.Error is System.IO.FileNotFoundException)
                        {
                            Message = "Url not found";
                            Code = "40401";
                            Status = "404";
                            context.Response.StatusCode = 404;
                        }

                        var jsonString = $"{{\"Status\":{Status},\"Code\":\"{Code}\",\"Message\":\"{Message}\"}}";
                        byte[] data = Encoding.UTF8.GetBytes(jsonString);
                        await context.Response.Body.WriteAsync(data, 0, data.Length);
                    });
                });
                //The default HSTS value is 30 days.You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.Use(async (context, next) =>
            {
                context.Request.EnableBuffering();
                await next();
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseStatusCodePages(async context =>
            {
                string Message = Microsoft.AspNetCore.WebUtilities.ReasonPhrases.GetReasonPhrase(context.HttpContext.Response.StatusCode);
                string Code = context.HttpContext.Response.StatusCode.ToString() + "01";
                string Status = context.HttpContext.Response.StatusCode.ToString();
                context.HttpContext.Response.ContentType = "application/json";
                var jsonString = $"{{\"Status\":{Status},\"Code\":\"{Code}\",\"Message\":\"{Message}\"}}";
                byte[] data = System.Text.Encoding.UTF8.GetBytes(jsonString);
                await context.HttpContext.Response.Body.WriteAsync(data, 0, data.Length);

            });

            app.UseSwagger();
            app.UseReDoc(c =>
            {
                c.SpecUrl("../swagger/v1/swagger.json");
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", "ProductMerchant API");
            });

            app.UseSignatureChecker("", _connectionString, _inMemoryClient, logger);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
    
}
