﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OAuth.Middleware
{
    public class ClientViewModels
    {
        public Guid clientId { get; set; }
        public string clientName { get; set; }
        public string clientKey { get; set; }
        public string clientSecret { get; set; }
        public int clientStatus { get; set; }
    }
}
