﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace OAuth.Middleware
{
    public static class StringCipher
    {
        // This constant is used to determine the keysize of the encryption algorithm in bits.
        // We divide this by 8 within the code below to get the equivalent number of bytes.
        private const int Keysize = 256;

        // This constant determines the number of iterations for the password bytes generation function.
        private const int DerivationIterations = 1000;

        public static string Encrypt(string plainText, string passPhrase)
        {
            // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
            // so that the same Salt and IV values can be used when decrypting.  
            var saltStringBytes = Generate256BitsOfRandomEntropy();
            var ivStringBytes = Generate256BitsOfRandomEntropy();
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        public static string Decrypt(string cipherText, string passPhrase)
        {
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
            // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
            var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        private static byte[] Generate256BitsOfRandomEntropy()
        {
            var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }
    }

    public class MD5Crypto
    {
        public static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        public static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string sha256(string text)
        {
            SHA256Managed crypt = new SHA256Managed();
            StringBuilder hash = new StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(text), 0, Encoding.UTF8.GetByteCount(text));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }

        public static string Md5(string text)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                return GetMd5Hash(md5Hash, text);
            }
        }

    }

    public static class HMACwithSHA256
    {
        public static string GetHMAC(string textPayload, string key)
        {
            var textBytes = Encoding.UTF8.GetBytes(textPayload);
            var keyBytes = Encoding.UTF8.GetBytes(key);
            Byte[] hashBytes;
            using (HMACSHA256 hmac = new HMACSHA256(keyBytes))
            {
                hashBytes = hmac.ComputeHash(textBytes);
            }
            return BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
        }

        public static bool VerifyHmacHash(string textPayload, string key, string hmac)
        {
            string hashOfText = GetHMAC(textPayload, key);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfText, hmac))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public static class RSAwithSHA256
    {
        public static string GetRSA(string textPayload, string privateKey)
        {
            string signature;
            using (RSACryptoServiceProvider privateKeyRsa = new RSACryptoServiceProvider())
            {
                privateKeyRsa.FromXmlString(privateKey);
                byte[] requestBytes = Encoding.UTF8.GetBytes(textPayload);
                Byte[] sign = privateKeyRsa.SignData(requestBytes, new SHA256CryptoServiceProvider());
                signature = Convert.ToBase64String(sign);
            }
            return signature;
        }

        public static bool VerifyRSA(string textPayload, string publicKey, string signature)
        {
            bool signed;
            using (RSACryptoServiceProvider publicKeyRsa = new RSACryptoServiceProvider())
            {
                publicKeyRsa.FromXmlString(publicKey);
                byte[] responseBytes = Encoding.UTF8.GetBytes(textPayload);
                byte[] signedBytes = Convert.FromBase64String(signature);
                signed = publicKeyRsa.VerifyData(responseBytes, "SHA256", signedBytes);
            }
            return signed;
        }
    }

    public static class RSAcrypto
    {
        public static string Encrypt(string textPayload, string publicKey)
        {
            string signature;
            using (RSACryptoServiceProvider publicKeyRsa = new RSACryptoServiceProvider())
            {
                publicKeyRsa.FromXmlString(publicKey);
                byte[] requestBytes = Encoding.UTF8.GetBytes(textPayload);
                Byte[] sign = publicKeyRsa.Encrypt(requestBytes, false);
                signature = Convert.ToBase64String(sign);
            }
            return signature;
        }

        public static string Decrypt(string signature, string privateKey)
        {
            string textPayload;
            using (RSACryptoServiceProvider privateKeyRsa = new RSACryptoServiceProvider())
            {
                privateKeyRsa.FromXmlString(privateKey);
                byte[] requestBytes = Convert.FromBase64String(signature);
                Byte[] sign = privateKeyRsa.Decrypt(requestBytes, false);
                textPayload = Encoding.UTF8.GetString(sign);
            }
            return textPayload;
        }
    }
}
