﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http.Extensions;

namespace OAuth.Middleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    // signature generator: https://8gwifi.org/rsasignverifyfunctions.jsp
    // key converter from pem to xml: https://superdry.apphb.com/tools/online-rsa-key-converter
    public class SignatureChecker
    {
        private readonly RequestDelegate _next;
        private readonly SignatureData _option;
        private ILog logger;

        public SignatureChecker(RequestDelegate next, IOptions<SignatureData> option)
        {
            _next = next;
            _option = option.Value;
            logger = option.Value.Logger;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            string requestId = httpContext.Response.Headers["RequestId"].FirstOrDefault();
            string id = httpContext.Request.Headers["x-api-key"];
            string signature = httpContext.Request.Headers["signature"];

            logger.Information($"ID:{requestId} From:{httpContext.Connection.RemoteIpAddress} Method:{httpContext.Request.Method} Path:{httpContext.Request.Path} Key:{id} Invoke Signature:{signature}");
            string url = UriHelper.GetEncodedUrl(httpContext.Request);
            if (!url.Contains("/api/"))
            {
                //non api. bypass
                await _next(httpContext);
                return;
            }

            int status = 200;
            string code = "20001";
            string message = "Success";
            string signstatus = "ok";
            string clientname = "";
            string clientkey = ""; 
            Guid gId = Guid.Empty;

            if (string.IsNullOrEmpty(id))
            {
                status = 401;
                code = "40102";
                message = "Missing client Id";
                signstatus = "error";
            }
            else
            {
                Guid.TryParse(id, out gId);

                ApplicationInMemoryClient client = _option.InMemoryClient.FirstOrDefault(c => c.clientId == gId);
                if (client == null)
                {
                    status = 401;
                    code = "40101";
                    message = "Invalid client Id";
                    signstatus = "invalid";
                }
                else
                {
                    clientname = client.clientName;
                    clientkey = client.clientKey;
                    //  Read the stream as text
                    string bodytxt = await new System.IO.StreamReader(httpContext.Request.Body).ReadToEndAsync();
                    //  Set the position of the stream to 0 to enable rereading
                    httpContext.Request.Body.Position = 0;
                    
                    if (string.IsNullOrEmpty(signature))
                    {
                        status = 401;
                        code = "40103";
                        message = "Missing signature";
                        signstatus = "error";
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(bodytxt))
                            bodytxt = url;
                        bodytxt = System.Text.RegularExpressions.Regex.Replace(bodytxt, @"\t|\n|\r", "");
                        if (RSAwithSHA256.VerifyRSA(bodytxt, clientkey, signature))
                        {
                            logger.Information($"ID:{requestId} From:{httpContext.Connection.RemoteIpAddress} Method:{httpContext.Request.Method} Path:{httpContext.Request.Path} Key:{httpContext.Request.Headers["x-api-key"]} SignStatus:{signstatus}");
                            httpContext.Response.Headers.Add("Client", clientname);
                            httpContext.Response.Headers.Remove("SignStatus");
                            httpContext.Response.Headers.Add("SignStatus", signstatus);
                            await _next(httpContext);
                            return;
                        }
                        else
                        {
                            status = 401;
                            code = "40104";
                            message = "Invalid signature";
                            signstatus = "invalid";
                        }
                    }
                }
            }

            logger.Error($"ID:{requestId} From:{httpContext.Connection.RemoteIpAddress} Method:{httpContext.Request.Method} Path:{httpContext.Request.Path} Key:{httpContext.Request.Headers["x-api-key"]} {code}:{message}");
            httpContext.Response.Headers.Add("Client", clientname);
            httpContext.Response.Headers.Remove("SignStatus");
            httpContext.Response.Headers.Add("SignStatus", signstatus);
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = status;
            var jsonString = $"{{\"Status\":{status},\"Code\":\"{code}\",\"Message\":\"{message}\"}}";
            byte[] data = Encoding.UTF8.GetBytes(jsonString);
            await httpContext.Response.Body.WriteAsync(data, 0, data.Length);
            return;
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class SignatureCheckerExtensions
    {
        public static IApplicationBuilder UseSignatureChecker(this IApplicationBuilder builder, string key, string connectionString, List<ApplicationInMemoryClient> inMemoryClient, ILog logger)
        {
            return builder.UseMiddleware<SignatureChecker>(Options.Create(new SignatureData { Key = key, ConnectionString = connectionString, InMemoryClient = inMemoryClient, Logger=logger }));
        }
    }

    public class SignatureData
    {
        public string Key { get; set; }
        public string ConnectionString { get; set; }
        public List<ApplicationInMemoryClient> InMemoryClient { get; set; }
        public ILog Logger { get; set; }
    }

    public class ApplicationInMemoryClient
    {
        public string clientName { get; set; }
        public Guid clientId { get; set; }
        public string clientKey { get; set; }
    }
}
